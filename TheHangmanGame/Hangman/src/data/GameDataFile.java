package data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import components.AppDataComponent;
import components.AppFileComponent;

import java.io.IOException;
import java.nio.file.Path;

/**
 * @author Ritwik Banerjee, Jude Hokyoon Woo
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class GameDataFile implements AppFileComponent {

    //Introducing the dummy constructor
    public GameDataFile() {
    }

    public static final String TARGET_WORD  = "TARGET_WORD";
    public static final String GOOD_GUESSES = "GOOD_GUESSES";
    public static final String BAD_GUESSES  = "BAD_GUESSES";

    @Override
    public void saveData(AppDataComponent data, Path to) {

    }

    @Override
    public void loadData(AppDataComponent data, Path from) throws IOException {

    }

    /** This method will be used if we need to export data into other formats. */
    @Override
    public void exportData(AppDataComponent data, Path filePath) throws IOException { }
}
