package data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Set;

/**
 * @author Jude Hokyoon Woo on 9/25/2016.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonGameData {
    //Introducing the dummy constructor
    public JsonGameData() {
    }

    private String targetWord;
    private Set<Character> goodGuesses;
    private Set<Character> badGuesses;
    private int remainingGuesses;


    public JsonGameData(String targetWord, Set<Character> goodGuesses, Set<Character> badGuesses, int remainingGuesses) {
        this.targetWord = targetWord;
        this.goodGuesses = goodGuesses;
        this.badGuesses = badGuesses;
        this.remainingGuesses = remainingGuesses;
    }

    public String gettargetWord() {
        return targetWord;
    }

    public void settargetWord(String targetWord) {
        this.targetWord = targetWord;
    }

    public Set<Character> getGoodGuesses() {
        return goodGuesses;
    }

    public void setGoodGuesses(Set<Character> goodGuesses) {
        this.goodGuesses = goodGuesses;
    }

    public Set<Character> getBadGuesses() {
        return badGuesses;
    }

    public void setBadGuesses(Set<Character> badGuesses) {
        this.badGuesses = badGuesses;
    }

    public int getRemainingGuesses() {
        return remainingGuesses;
    }

    public void setRemainingGuesses(int remainingGuesses) {
        this.remainingGuesses = remainingGuesses;
    }

}
