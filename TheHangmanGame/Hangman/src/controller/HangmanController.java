package controller;

import apptemplate.AppTemplate;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.xml.internal.fastinfoset.sax.SystemIdResolver;
import com.sun.xml.internal.fastinfoset.util.StringArray;
import data.GameData;
import data.JsonGameData;
import gui.Workspace;
import javafx.animation.AnimationTimer;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.file.Path;

import static settings.AppPropertyType.*;

/**
 * @author Ritwik Banerjee, Jude Hokyoon Woo
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class HangmanController implements FileController {

    private static AppTemplate appTemplate; // shared reference to the application
    private static GameData gamedata;    // shared reference to the game being played, loaded or saved
    private Text[] progress;    // reference to the text area for the word
    private boolean success;     // whether or not player was successful
    private int discovered;  // the number of letters already discovered
    private static Button gameButton;  // shared reference to the "start game" button
    private Label remains;     // dynamically updated label that indicates the number of remaining guesses
    private boolean gameover;    // whether or not the current game is already over
    private boolean savable;
    private Path workFile;
    private String jsondataName;

    public HangmanController(AppTemplate appTemplate, Button gameButton) {
        this(appTemplate);
        this.gameButton = gameButton;
    }

    public HangmanController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
    }

    public void enableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(false);
    }

    public void start() {
        gamedata = new GameData(appTemplate);
        gameover = false;
        success = false;
        savable = true;
        discovered = 0;
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        appTemplate.getGUI().updateWorkspaceToolbar(savable);
        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        HBox guessedLetters = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);

        remains = new Label(Integer.toString(GameData.TOTAL_NUMBER_OF_GUESSES_ALLOWED));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);
        initWordGraphics(guessedLetters);
        play();
    }

    private void end() {
        System.out.println(success ? "You win!" : "Ah, close but not quite there. The word was \"" + gamedata.getTargetWord() + "\".");
        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null);
        gameover = true;
        gameButton.setDisable(true);
        savable = false; // cannot save a game that is already over
        appTemplate.getGUI().updateWorkspaceToolbar(savable);

    }

    private void initWordGraphics(HBox guessedLetters) {
        char[] targetword = gamedata.getTargetWord().toCharArray();
        progress = new Text[targetword.length];
        for (int i = 0; i < progress.length; i++) {
            progress[i] = new Text(Character.toString(targetword[i]));
            progress[i].setVisible(false);
        }
        guessedLetters.getChildren().addAll(progress);
    }

    public void play() {
        AppMessageDialogSingleton messageDialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager propertyManager = PropertyManager.getManager();


        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                appTemplate.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) -> {
                    if (event.getCharacter().matches("[a-z]") || event.getCharacter().matches("'")) {
                        char guess = event.getCharacter().charAt(0);

                        if (!alreadyGuessed(guess)) {
                            boolean goodguess = false;
                            for (int i = 0; i < progress.length; i++) {
                                if (gamedata.getTargetWord().charAt(i) == guess) {
                                    progress[i].setVisible(true);
                                    gamedata.addGoodGuess(guess);
                                    goodguess = true;
                                    discovered++;
                                }
                            }
                            if (!goodguess)
                                gamedata.addBadGuess(guess);

                            success = (discovered == progress.length);
                            remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
                        }
                    } else if (!event.getCharacter().matches("[a-z]") && !event.getCharacter().matches("'")) {
                        messageDialog.show(propertyManager.getPropertyValue(WRONG_KEY_TITLE), propertyManager.getPropertyValue(WRONG_KEY_MESSAGE));
                    }

                });
                if (gamedata.getRemainingGuesses() <= 0 || success)
                    stop();
            }

            @Override
            public void stop() {
                super.stop();
                end();
            }
        };
        timer.start();
    }

    private boolean alreadyGuessed(char c) {
        return gamedata.getGoodGuesses().contains(c) || gamedata.getBadGuesses().contains(c);
    }

    @Override
    public void handleNewRequest() {
        AppMessageDialogSingleton messageDialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager propertyManager = PropertyManager.getManager();
        boolean makenew = true;
        if (savable)
            try {
                makenew = promptToSave();
            } catch (IOException e) {
                messageDialog.show(propertyManager.getPropertyValue(NEW_ERROR_TITLE), propertyManager.getPropertyValue(NEW_ERROR_MESSAGE));
            }
        if (makenew) {
            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated
            workFile = null;                                       // new workspace has never been saved to a file

            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
            enableGameButton();

            messageDialog.show(propertyManager.getPropertyValue(NEW_COMPLETED_TITLE), propertyManager.getPropertyValue(NEW_COMPLETED_MESSAGE));
        }

        if (gameover) {
            savable = false;
            appTemplate.getGUI().updateWorkspaceToolbar(savable);
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
            enableGameButton();
        }

    }

    @Override
    public void handleSaveRequest() throws IOException {
        AppMessageDialogSingleton messageDialog = AppMessageDialogSingleton.getSingleton();
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();
        PropertyManager propertyManager = PropertyManager.getManager();

        yesNoCancelDialog.show(propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_TITLE), propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE));
        if (YesNoCancelDialogSingleton.YES.equals(yesNoCancelDialog.getSelection())) {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle(propertyManager.getPropertyValue(SAVE_WORK_TITLE));
            fileChooser.setInitialFileName(propertyManager.getPropertyValue(WORK_FILE_EXT_DESC));
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Json File(*.json)", "*.json"));
            File file = fileChooser.showSaveDialog(yesNoCancelDialog);
            if (file == null) {
                return;
            }
            ObjectMapper mapper = new ObjectMapper();
            JsonGameData mygamedata = new JsonGameData(gamedata.getTargetWord(), gamedata.getGoodGuesses(), gamedata.getBadGuesses(), gamedata.getRemainingGuesses());
            jsondataName = file.getAbsolutePath();

            try {
                mapper.writeValue(new File(jsondataName), mygamedata);
                messageDialog.show(propertyManager.getPropertyValue(SAVE_COMPLETED_TITLE), propertyManager.getPropertyValue(SAVE_COMPLETED_MESSAGE));
                savable = true; //can save only one time at a game.
                appTemplate.getGUI().updateWorkspaceToolbar(savable);
                gameButton.setDisable(true);
            } catch (IOException e1) {
                messageDialog.show(propertyManager.getPropertyValue(SAVE_ERROR_TITLE), propertyManager.getPropertyValue(SAVE_ERROR_MESSAGE));
            }

        }
    }

    @Override
    public void handleLoadRequest() {
        AppMessageDialogSingleton messageDialog = AppMessageDialogSingleton.getSingleton();
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();
        PropertyManager propertyManager = PropertyManager.getManager();

        yesNoCancelDialog.show(propertyManager.getPropertyValue(LOAD_WORK_TITLE), propertyManager.getPropertyValue(LOAD_WORK_MESSAGE));

        if (YesNoCancelDialogSingleton.YES.equals(yesNoCancelDialog.getSelection())) {

            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle(propertyManager.getPropertyValue(LOAD_WORK_TITLE));
            File file = fileChooser.showOpenDialog(yesNoCancelDialog);
            ObjectMapper mapper = new ObjectMapper();

            if (file == null) {
                return;
            }
            try {
                gamedata = new GameData(appTemplate);
                gamedata.setTargetWord(mapper.readValue(file, GameData.class).getTargetWord());
                gamedata.setGoodGuesses(mapper.readValue(file, GameData.class).getGoodGuesses());
                gamedata.setBadGuesses(mapper.readValue(file, GameData.class).getBadGuesses());
                gamedata.setRemainingGuesses(mapper.readValue(file, GameData.class).getRemainingGuesses());
                gameover = false;
                success = false;
                savable = true;
                boolean makenew = true;

                Object[] goodguessArray = gamedata.getGoodGuesses().toArray();

                if (makenew) {
                    appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
                    appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
                    ensureActivatedWorkspace();                            // ensure workspace is activated
                    workFile = null;                                       // new workspace has never been saved to a file

                    Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
                    gameWorkspace.reinitialize();
                    gameButton.setDisable(true);
                }

                Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
                appTemplate.getGUI().updateWorkspaceToolbarLoad(savable);
                HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
                HBox guessedLetters = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);

                remains = new Label(Integer.toString(gamedata.getRemainingGuesses()));
                remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);
                initWordGraphics(guessedLetters);

                for (int j = 0; j < goodguessArray.length; j++) {
                    char guess = goodguessArray[j].toString().charAt(0);
                    boolean goodguess = false;
                    char[] targetword = gamedata.getTargetWord().toCharArray();
                    for (int i = 0; i < progress.length; i++) {
                        if (gamedata.getTargetWord().charAt(i) == guess) {
                            progress[i].setVisible(true);
                            gamedata.addGoodGuess(guess);
                            goodguess = true;
                            discovered++;
                        }
                    }
                }
                messageDialog.show(propertyManager.getPropertyValue(LOAD_COMPLETED_TITLE), propertyManager.getPropertyValue(LOAD_COMPLETED_MESSAGE));
                play();

                if (gameover) {
                    savable = false;
                    appTemplate.getGUI().updateWorkspaceToolbar(savable);
                    //Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
                    gameWorkspace.reinitialize();
                    enableGameButton();

                }

            } catch (IOException e1) {
                messageDialog.show(propertyManager.getPropertyValue(LOAD_ERROR_TITLE), propertyManager.getPropertyValue(LOAD_ERROR_MESSAGE));
            }


        }

    }

    @Override
    public void handleInfoRequest() {
        AppMessageDialogSingleton messageDialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager propertyManager = PropertyManager.getManager();
        messageDialog.show(propertyManager.getPropertyValue(INFO_TITLE), propertyManager.getPropertyValue(INFO_MESSAGE));
    }

    ;

    @Override
    public void handleExitRequest() {
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();
        PropertyManager props = PropertyManager.getManager();


        try {
            boolean exit = true;
            if (savable) {
                exit = promptToSave();
                yesNoCancelDialog.show(props.getPropertyValue(SAVE_UNSAVED_WORK_TITLE), props.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE));
                if (YesNoCancelDialogSingleton.YES.equals(yesNoCancelDialog.getSelection())) {
                    handleSaveRequest();
                } else if (YesNoCancelDialogSingleton.CANCEL.equals(yesNoCancelDialog.getSelection()) || YesNoCancelDialogSingleton.NO.equals(yesNoCancelDialog.getSelection())) {
                    exit = true;
                }
            }
            if (exit)
                System.exit(0);
        } catch (IOException ioe) {
            dialog.show(props.getPropertyValue(SAVE_ERROR_TITLE), props.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
    }

    private void ensureActivatedWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }

    private boolean promptToSave() throws IOException {
        return false; // dummy placeholder
    }

    /**
     * A helper method to save work. It saves the work, marks the current work file as saved, notifies the user, and
     * updates the appropriate controls in the user interface
     *
     * @param target The file to which the work will be saved.
     * @throws IOException
     */
    private void save(Path target) throws IOException {

    }
}
