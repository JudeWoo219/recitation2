package controller;

import java.io.IOException;

/**
 * @author Ritwik Banerjee, Jude Hokyoon Woo
 */
public interface FileController {

    void handleNewRequest();

    void handleInfoRequest();

    void handleSaveRequest() throws IOException;

    void handleLoadRequest() throws IOException;

    void handleExitRequest();
}
